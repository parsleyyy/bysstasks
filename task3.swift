/// Protocol specifies whether property must be settable/gettable.
protocol CanBeDone {
    var isDone: Bool { get set }
}

/// Class represents single "to do" item.
class CheckListElement : CanBeDone {
    /// Status of specific task.
    var isDone: Bool
    
    /// Name of task.
    var textField: String
    
    /// Target day for task. 
    var weekDay: String
    
    /// Resets task status.
    func isDoneSwitch() {
        self.isDone =  !isDone
    }

    /// Initializes instance of todo list element with no given parameters.
    init () {
        self.isDone = false
        self.textField = "Be a good human"
        self.weekDay = "Indefinite"
    }
    
    /// Initializes instance of todo list element with given parameters.
    init (isDone: Bool, weekDay: String, textField: String) {
        self.isDone = isDone
        self.weekDay = weekDay
        self.textField = textField
    }
}

/// Extention which converts an instance to a string.
extension CheckListElement : CustomStringConvertible {
    var description : String {
        let status = self.isDone ? "Done" : "To be done"
        return "\(weekDay) \(textField) -> " + status
    }   
}

let element1 = CheckListElement(isDone: true, weekDay: "Saturday", textField: "Go shopping")
let element2 = CheckListElement()

element1.isDoneSwitch()

print("TASK 3")
print(element1)
print(element2)
