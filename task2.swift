/// Class represents single "to do" item.
class CheckListElement {
    /// Status of specific task.
    var isDone: Bool
    
    /// Name of task.
    var textField: String
    
    /// Target day for task. 
    var weekDay: String

    /// Initializes instance of todo list element with no given parameters.
    init () {
        self.isDone = false
        self.textField = "Be a good human"
        self.weekDay = "Indefinite"
    }
    
    /// Initializes instance of todo list element with given parameters.
    init (isDone: Bool, weekDay: String, textField: String) {
        self.isDone = isDone
        self.weekDay = weekDay
        self.textField = textField
    }
}

/// Extention which converts an instance to a string.
extension CheckListElement : CustomStringConvertible {
    var description : String {
        let status = self.isDone ? "Done" : "To be done"
        return "\(weekDay) \(textField) -> " + status
    }   
}

let element1 = CheckListElement(isDone: true, weekDay: "Saturday", textField: "Go shopping")
let element2 = CheckListElement()

print("TASK 2")
print(element1)
print(element2)
