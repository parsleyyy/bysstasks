/// Protocol made for communication between sender and receiver.
protocol CommunicationDelegate: AnyObject {
    /// Prints all list's elements.
    func printToDos()
}

/// Protocol specifies whether property must be settable/gettable.
protocol CanBeDone {
    var isDone: Bool { get set }
}

/// Class represents single "to do" item.
class CheckListElement : CanBeDone {
    weak var delegate: CommunicationDelegate?
    
    /// Status of specific task.
    var isDone: Bool
    
    /// Name of task.
    var textField: String
    
    /// Target day for task. 
    var weekDay: String
    
    /// Resets task status.
    func isDoneSwitch() {
        self.delegate?.printToDos()
        self.isDone =  !isDone
    }
    
     /// Initializes instance of todo list element with no given parameters.
    init () {
        self.isDone = false
        self.textField = "Be a good human"
        self.weekDay = "Indefinite"
    }
    
    /// Initializes instance of todo list element with given parameters.
    init (isDone: Bool, weekDay: String, textField: String) {
        self.isDone = isDone
        self.weekDay = weekDay
        self.textField = textField
    }
    
}

/// Extention which converts an instance to a string.
extension CheckListElement : CustomStringConvertible {
    var description : String {
        let status = self.isDone ? "Done" : "To be done"
        return "\(weekDay) \(textField) -> " + status
    }   
}

/// Class represents "todo" list.
class CheckList : CommunicationDelegate {
    /// List of checklist items.
    var listElements = [CheckListElement]()
    
     /// Initializes instance of list's element with given parameter.
    init(listElements: [CheckListElement] = []) {
        self.listElements = listElements
    }
    
    /// Prints all todos.
    func printToDos() {
        for item in self.listElements {
            print(item)
        }
    }
    
    /// Prints every third task.
    func printEveryThirdToDo() {
        for index in self.listElements.indices.dropFirst() {
            if (0 == (index + 1) % 3) {
                print(self.listElements[index])
            }
        }
    }
}

let element1 = CheckListElement(isDone: true, weekDay: "Saturday", textField: "Go shopping")
let element2 = CheckListElement()
let element3 = CheckListElement(isDone: false, weekDay: "Sunday", textField: "Lay in bed")
let element4 = CheckListElement(isDone: true, weekDay: "Friday", textField: "Eat a lot")
let element5 = CheckListElement(isDone: false, weekDay: "Tuesday", textField: "Learn hard")
let element6 = CheckListElement()
let element7 = CheckListElement(isDone: true, weekDay: "Wednesday", textField: "Code")
let element8 = CheckListElement(isDone: false, weekDay: "Tuesday", textField: "Learn hard")
let element9 = CheckListElement(isDone: true, weekDay: "Wednesday", textField: "Sleep")
let element10 = CheckListElement(isDone: true, weekDay: "Saturday", textField: "One day more with code")

element3.isDoneSwitch()

let receiver = CheckList(listElements: [element1, element2, element3, element4, element5, element6, element7, element8, element9, element10])

for item in receiver.listElements {
  item.delegate = receiver
}

print("Every third elements:")
receiver.printEveryThirdToDo()
