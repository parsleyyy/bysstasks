/// Protocol made for communication between sender and receiver.
protocol CommunicationDelegate: AnyObject {
    /// Prints all list's elements.
    func printToDos()
}

/// Protocol specifies whether property must be settable/gettable.
protocol CanBeDone {
    var isDone: Bool { get set }
}

/// Class represents single "to do" item.
class CheckListElement : CanBeDone {
    weak var delegate: CommunicationDelegate?
    
    /// Status of specific task.
    var isDone: Bool
    
    /// Name of task.
    var textField: String
    
    /// Target day for task. 
    var weekDay: String
    
    /// Resets task status.
    func isDoneSwitch() {
        self.delegate?.printToDos()
        self.isDone =  !isDone
    }
    
     /// Initializes instance of todo list element with no given parameters.
    init () {
        self.isDone = false
        self.textField = "Be a good human"
        self.weekDay = "Indefinite"
    }
    
    /// Initializes instance of todo list element with given parameters.
    init (isDone: Bool, weekDay: String, textField: String) {
        self.isDone = isDone
        self.weekDay = weekDay
        self.textField = textField
    }
    
}

/// Extention which converts an instance to a string.
extension CheckListElement : CustomStringConvertible {
    var description : String {
        let status = self.isDone ? "Done" : "To be done"
        return "\(weekDay) \(textField) -> " + status
    }   
}

/// Class represents "todo" list.
class CheckList : CommunicationDelegate {
    /// List of checklist items.
    var listElements = [CheckListElement]()
    
     /// Initializes instance of list's element with given parameter.
    init(listElements: [CheckListElement] = []) {
        self.listElements = listElements
    }
    
    /// Prints all todos.
    func printToDos() {
        for item in self.listElements {
            print(item)
        }
    }
}
