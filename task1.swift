/// Class represents single "to do" item.
class CheckListElement {
    /// Status of specific task.
    var isDone: Bool
    
    /// Name of task.
    var textField: String
    
    /// Initializes instance of todo list element with no given parameters.
    init () {
        self.isDone = false
        self.textField = "Be a good human"
    }
    
    /// Initializes instance of todo list element with given parameters.
    init (isDone: Bool, textField: String) {
        self.isDone = isDone
        self.textField = textField
    }
}

/// Extention which converts an instance to a string.
extension CheckListElement : CustomStringConvertible {
    var description : String {
        let status = self.isDone ? "Done" : "To be done"
        return "Day of week \(textField) -> " + status
    }   
}

let element1 = CheckListElement(isDone: true, textField: "Cleanings")
let element2 = CheckListElement()

print("TASK 1")
print(element1)
print(element2)
